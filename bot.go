package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
)

type track struct {
	Albumid   string
	AlbumName string
	Artistid  string
	Artist    string
	Title     string
	Lyric     string
	Trackid   string
	AlbumDate string
}

/*
	UPDATE tracks info
*/
func updateTracks(artist string) {
	// Load the HTML document
	doc, err := goquery.NewDocument("https://music.bugs.co.kr/search/track?q=" + artist)
	if err != nil {
		log.Fatal(err)
	}

	tracks := []track{}

	// Find the review items
	doc.Find(".list.trackList tbody tr").Each(func(i int, s *goquery.Selection) {

		// For each item found, get the title
		albumid, _ := s.Attr("albumid")
		artistid, _ := s.Attr("artistid")
		trackid, _ := s.Attr("trackid")
		// if "20047596" != artistid {
		// 	return
		// }
		albumname, _ := s.Find("td a.album").Attr("title")

		title, _ := s.Find(".check input").First().Attr("title")
		fmt.Printf("Track %d: %s %s %s %s\n", i+1, albumid, title, trackid, albumname)

		lyricDoc, err := goquery.NewDocument("https://music.bugs.co.kr/track/" + trackid)
		if err != nil {
			log.Fatal(err)
		}
		lyric := lyricDoc.Find(".lyricsContainer xmp").Text()

		albumInfo, err := goquery.NewDocument("https://music.bugs.co.kr/album/" + albumid)
		if err != nil {
			log.Fatal(err)
		}
		var albumDate string
		albumInfo.Find(".summaryAlbum .basicInfo .info tr").Each(func(i int, tr *goquery.Selection) {
			th := tr.Find("th").Text()
			if th == "발매일" {
				albumDate = tr.Find("time").Text()
			}
		})

		track := track{
			Albumid:   albumid,
			AlbumName: albumname,
			Trackid:   trackid,
			Title:     title,
			Artistid:  artistid,
			Lyric:     lyric,
			Artist:    artist,
			AlbumDate: albumDate,
		}

		tracks = append(tracks, track)
	})

	b, err := json.Marshal(&tracks)
	fmt.Println(string(b))

	_ = ioutil.WriteFile("tracks.json", b, 0644)
}

func randomTrack() (track, int) {
	file, _ := ioutil.ReadFile("tracks.json")

	tracks := []track{}

	_ = json.Unmarshal([]byte(file), &tracks)

	var rand int
	var selected track
	for {
		rand = randomNumber(len(tracks))
		selected = tracks[rand]
		if len(strings.TrimSpace(selected.Lyric)) > 0 {
			break
		}
	}

	return selected, rand
}

func randomNumber(max int) int {
	randomIndex := rand.Intn(max-0) + 0
	return randomIndex
}

func main() {
	if _, err := os.Stat("tracks.json"); err == nil {
		// fmt.Println("track.json file exists")

		if len(os.Args) == 2 {
			// update tracks.json
			fmt.Println("Load lyrics for artist: " + os.Args[1])
			fmt.Println("====== Start loading lyrics ======")
			updateTracks(os.Args[1])
			fmt.Println("====== End loading lyrics ======")

			os.Exit(0)
		}

	} else {
		fmt.Println("track.json file not exists")
		if len(os.Args) == 1 {
			fmt.Println("missing argument, run ./bot [artist]")
		} else {
			fmt.Println("Load lyrics for artist: " + os.Args[1])
			fmt.Println("====== Start loading lyrics ======")
			updateTracks(os.Args[1])
			fmt.Println("====== End loading lyrics ======")
		}

		os.Exit(0)
	}

	rand.Seed(time.Now().UnixNano())

	selectedTrack, randomTrackIdx := randomTrack()
	temp := strings.Split(selectedTrack.Lyric, "\n")

	randomIndex := randomNumber(len(temp))
	randomTrackIdx++
	// key := strconv.Itoa(randomTrackIdx) + "-" + strconv.Itoa(randomIndex)
	fmt.Println("Lyrics:")
	fmt.Println(temp[randomIndex])
	fmt.Println()
	fmt.Println("Album info:")
	fmt.Println("Artist:", selectedTrack.Artist)
	fmt.Println("Title:", selectedTrack.Title)
	fmt.Println("Album:", selectedTrack.AlbumName)
	fmt.Println("Album Release Date:", selectedTrack.AlbumDate)
}
