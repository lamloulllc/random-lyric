# Random a single line of lyrics from an artist

### Build

run `go mod tidy` for the first time if go.sum file not exist

`go build bot.go`

### Run 

1. To fetch lyrics of an artist

    For the first time or whenever you want to change artist or update track info, input with the name of artist and the bot will fetch lyrics from music site, 
    
    run    
    `./bot [artist]`

    e.g.:
    `./bot OneRepublic`

2. Output random lyric

    run
    
    `./bot`

    Output 1:

    ```
    Lyrics:
    'Til the love runs out, 'til the love runs out

    Album info:
    Artist: OneRepublic
    Title: Love Runs Out
    Album: Native
    Album Release Date: 2014.01.01
    ```

    Output 2:
    ```
    Lyrics:
    Sink in the river the lessons I learned

    Album info:
    Artist: OneRepublic
    Title: Counting Stars
    Album: Native
    Album Release Date: 2013.01.01
    ```
    
### Disclaimer

All data of artist, songs, album and lyrics are getting from https://music.bugs.co.kr/