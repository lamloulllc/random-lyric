module random-lyrics/bot

go 1.18

require github.com/PuerkitoBio/goquery v1.8.1 // direct

require (
	github.com/andybalholm/cascadia v1.3.2 // indirect
	golang.org/x/net v0.10.0 // indirect
)
